import React from 'react';
import './App.css';
import WeatherScreen from "./screens/WeatherScreen";
import {MainScreen} from "./screens/MainScreen";
import {Menu} from "./components/Menu";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

const App = () => {
  return (
    <div>
      <Router>
        <Menu/>
        <Switch>
          <Route exact path="/">
            <MainScreen/>
          </Route>
          <Route path="/weather/:city">
            <WeatherScreen/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
};

export default App;
