import React from 'react'
import {Weather} from "../components/Weather";
import {Search} from "../components/Search";
import {connect} from "react-redux";
import {IAppState, WeatherState} from "../store/state";
import {compose, Dispatch} from "redux";
import {setLoading, setWeather} from "../store/actions";
import {match, withRouter} from "react-router";
import {Loader} from "../components/Loader/Loader";

export interface Data {
  main: {
    temp: string;
    pressure: number;
    humidity: number;

  };
  sys: {
    sunset: number;
    sunrise: number;
  };
  wind: {
    speed: number;
  };
  weather: Array<IWeather>;
  name: string;
}

interface IWeather {
  icon: string;
  description: string;
}

interface StateProps {
  isLoading: boolean;
  weather: WeatherState;
}

interface DispatchProps {
  setLoading(isLoading: boolean): void;

  setWeather(weather: WeatherState): void;
}

interface OuterProps {
  match: match<{ city: string }>
}

type Props = StateProps & DispatchProps & OuterProps;

class WeatherScreen extends React.Component<Props> {
  componentDidUpdate(prevProps: Props) {
    if (this.props.weather.city && prevProps.weather.city !== this.props.weather.city) {
      this.loadData();
    }
  }

  componentDidMount() {
    this.props.setWeather({
      city: this.props.match.params.city,
      type: 'city',
    });
  }


  render(): React.ReactNode {
    if (this.props.isLoading) {
      return <Loader/>
    }


    return (
      <div>
        <Search onSubmitCity={this.changeCity} onReset={this.resetData} onSubmitCoordinates={this.changeCoordinate}/>
        {this.props.weather.data &&
        <Weather
            data={this.props.weather.data}
        />
        }
      </div>
    );
  }

  changeCity = (city: string) => {
    const {setWeather, weather} = this.props;
    setWeather({
      ...weather,
      city: city,
      data: undefined,
      type: 'city',
    });
    this.loadData();
  };

  changeCoordinate = (coordinatesLon: string, coordinatesLat: string) => {
    const {setWeather, weather} = this.props;
    setWeather({
      ...weather,
      coordinatesLon: coordinatesLon,
      coordinatesLat: coordinatesLat,
      data: undefined,
      type: 'coordinate',
    });
    this.loadData();
  };

  resetData = () => {
    const {setLoading, setWeather, weather} = this.props;

    setLoading(false);

    setWeather({
      ...weather,
      city: "",
      data: undefined,
    });
  };

  loadData = () => {
    const {setLoading, setWeather, weather} = this.props;

    setLoading(true);
    let url = '';

    if (this.props.weather.type == 'city') {
      url = 'https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/weather?q=' + (this.props.weather.city) + ',cz&APPID=39dd94ae8f36533af5f96abafb5ef929';
    } else if (this.props.weather.type == 'coordinate') {
      url = 'https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/weather?lat=' + (this.props.weather.coordinatesLat) + '&' + 'lon=' + (this.props.weather.coordinatesLon) + ',cz&APPID=39dd94ae8f36533af5f96abafb5ef929';
    } else throw Error('Nefunguje to');

    fetch(url)
      .then(res => res.json())
      .then(json => {
        setWeather({
          ...weather,
          data: json,
        });
        setLoading(false);
      })
      .catch(error => {
        throw new Error('Nefunguje to');
      })
  }
}

const mapStateToProps = (state: IAppState): StateProps => ({
  isLoading: state.isLoading,
  weather: state.weather,
});

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  setLoading: (isLoading: boolean) => dispatch(setLoading(isLoading)),
  setWeather: (weather: WeatherState) => dispatch(setWeather(weather)),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(WeatherScreen) as React.ComponentClass;
