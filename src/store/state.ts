import {Data} from "../screens/WeatherScreen";

export interface IAppState {
  isLoading: boolean;
  weather: WeatherState,
}

export const initialState: IAppState = {
  isLoading: false,
  weather: {
    city: null,
    type: 'city',
  }
};

export interface WeatherState {
  city: string | null;
  type: string;
  data?: Data;
  coordinatesLat?: string,
  coordinatesLon?: string,
}
