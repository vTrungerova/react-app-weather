import {ActionType} from "./actions";
import {initialState} from "./state";

export default function (state = initialState, action: any) {
  switch (action.type) {
    case ActionType.SET_LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    case ActionType.SET_WEATHER:
      return {
        ...state,
        weather: action.weather
      };

    default:
      return state;
  }
}
