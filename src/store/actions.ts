import {WeatherState} from "./state";

export const ActionType = {
  SET_LOADING: 'SET_LOADING',
  SET_WEATHER: 'SET_WEATHER',
};

export function setLoading(isLoading: boolean) {
  return {
    type: ActionType.SET_LOADING,
    isLoading: isLoading,
  };
}

export function setWeather(weather: WeatherState) {
  return {
    type: ActionType.SET_WEATHER,
    weather: weather,
  }
}
