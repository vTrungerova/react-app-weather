import React from 'react';

export const Loader = () => (
  <div className={'loader-wrapper'}>
    <div className={'lds-hourglass'}></div>
  </div>
);

