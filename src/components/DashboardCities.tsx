import React from 'react'
import './style.css'
import {Link} from "react-router-dom";

export class DashboardCities extends React.Component {
  cities = ['Vsetin', 'Praha', 'Ostrava', 'Brno'];

  render() {
    return(
      <div>
        {this.cities.map(function (city, index) {
          return (
            <div className={'dashboard-item'}>
              <Link to={`/weather/${city}`} key={index}>{city}</Link>
            </div>
          )
        })}
      </div>
    )
  }
}
