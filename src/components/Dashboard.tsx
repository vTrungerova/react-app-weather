import React from 'react'
import './style.css'
import {DashboardCities} from "./DashboardCities";

export class Dashboard extends React.Component {
  render() {
    return(
      <div>
        <h1>Hlavni stranka</h1>
        <DashboardCities/>
      </div>
    )
  }
}
