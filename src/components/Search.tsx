import React from 'react'
import './style.css'

interface Props {
    onSubmitCity(city: string): void;
    onReset(): void;
    onSubmitCoordinates(coordinatesLon: string, coordinatesLat: string): void;
}

export function Search(props: Props) {
    const [city, setCity] = React.useState('');
    const [coordinatesLon, setCoordinatesLon] = React.useState('');
    const [coordinatesLat, setCoordinatesLat] = React.useState('');
    const [type, setType] = React.useState('');

    const {onSubmitCity} = props;
    const {onSubmitCoordinates} = props;
    const {onReset} = props;

    const submit = () => {
        if (type == 'city') {
            onSubmitCity(city);
        } else if (type == 'coordinates') {
            onSubmitCoordinates(coordinatesLon, coordinatesLat);
        }
    };

    const reset = () => {
        onReset();
    };

    return(
        <div>

            <select onChange={e => setType(e.target.value)}>
               <option value={'city'}>Vyhledat podle mesta</option>
               <option value={'coordinates'}>Vyhledat podle souradnic</option>
            </select>
            {type == 'city' &&
            <div>
                <input onChange={e => setCity(e.target.value)}/>
            </div>
            }
            {type == 'coordinates' &&
            <div>
                <input onChange={e => setCoordinatesLon(e.target.value)}/>
                <input onChange={e => setCoordinatesLat(e.target.value)}/>
            </div>
            }

            <button onClick={submit}>Odeslat</button>
            <button onClick={reset}>Reset</button>

        </div>
    );
}
