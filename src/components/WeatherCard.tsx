import React, {useState} from 'react'
import {Data} from "../screens/WeatherScreen";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {Table} from "react-bootstrap";
import moment from "moment";
import Card from "react-bootstrap/Card";
import {Button} from "./Button";

interface Props {
  data: Data;
}

export const WeatherCard: React.FC<Props> = (props) => {
  const {data} = props;
  let kelvin = parseFloat(data.main.temp);
  let celsius = Math.round(kelvin - 273.15);
  let day = moment().locale('en').format('dddd');
  let sunrise = moment.unix(data.sys.sunrise).locale('cs').format('LT');
  let sunset = moment.unix(data.sys.sunset).locale('cs').format('LT');

  const [isOpen, setOpen] = useState(false);

  return (
    <Card bg="light" style={{width: '18rem'}}>
      <Card.Header>
        <Row>
          <Col className={'day'}>{day}</Col>
          <Col className={'temperature'}>{celsius} °C</Col>
        </Row>
      </Card.Header>
      <Card.Img variant="top" src={'http://openweathermap.org/img/wn/' + data.weather[0].icon + '@2x.png'}/>
      {isOpen ? <Button onClick={() => setOpen(!isOpen)}>Zavrit</Button> :
        <Button onClick={() => setOpen(!isOpen)}>Otevrit</Button>}

      {isOpen &&
      <Card.Body>
          <Card.Text>
              <Table>
                  <tr>
                      <td>Wind</td>
                      <td>{data.wind.speed} m/s</td>
                  </tr>
                  <tr>
                      <td>Cloudiness</td>
                      <td>{data.weather[0].description}</td>
                  </tr>
                  <tr>
                      <td>Pressure</td>
                      <td>{data.main.pressure} hpa</td>
                  </tr>
                  <tr>
                      <td>Humidity</td>
                      <td>{data.main.humidity} %</td>
                  </tr>
                  <tr>
                      <td>Sunrise</td>
                      <td>{sunrise}</td>
                  </tr>
                  <tr>
                      <td>Sunset</td>
                      <td>{sunset}</td>
                  </tr>
              </Table>
          </Card.Text>
      </Card.Body>
      }
    </Card>
  )
}
