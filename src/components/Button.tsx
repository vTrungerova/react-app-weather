import React, {ReactNode} from 'react';

interface ButtonProps {
  onClick(): void;
  children: ReactNode;
}

export const Button = (props: ButtonProps) => (
  <button onClick={props.onClick}>
    {props.children}
  </button>
);

