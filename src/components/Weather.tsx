import React from 'react'
import './style.css'
import 'moment/locale/cs.js'
import 'moment/locale/en-gb.js'
import {WeatherCard} from "./WeatherCard";
import {Data} from "../screens/WeatherScreen";


interface Props {
  data: Data;
}

export class Weather extends React.Component<Props> {


  render(): React.ReactNode {
    let {data} = this.props;
    return (
      <div className={'card-wrapper'}>
        <h1 className={'text-center'}>{data.name}</h1>
        <WeatherCard data={data}/>
      </div>
    );

  }
}
